import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class KeyEventService {
  private routeFunctionMap: { [key: string]: () => void } = {};

  constructor(private router: Router) {
    this.initializeKeyListener();
  }

  initializeKeyListener() {
    window.addEventListener('keydown', (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === 's') {
        event.preventDefault();
        this.executeRouteFunction();
      }
    });

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        // eslint-disable-next-line no-console
        console.log(`Navigated to: ${this.router.url}`);
      });
  }

  registerRouteFunction(routePattern: string, func: () => void) {
    this.routeFunctionMap[routePattern] = func;
  }

  executeRouteFunction() {
    const currentRoute = this.router.url;
    const matchedRoute = this.matchRoutePattern(currentRoute);
    if (matchedRoute) {
      this.routeFunctionMap[matchedRoute]();
    } else {
      // eslint-disable-next-line no-console
      console.warn(`No function registered for route: ${currentRoute}`);
    }
  }

  matchRoutePattern(currentRoute: string): string | undefined {
    return Object.keys(this.routeFunctionMap).find(pattern => {
      const regexPattern = new RegExp(
        `^${pattern.replace(/:[^\s/]+/g, '([^/]+)')}$`,
      );
      return regexPattern.test(currentRoute);
    });
  }
}
