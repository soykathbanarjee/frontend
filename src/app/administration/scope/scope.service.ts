import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, switchMap } from 'rxjs';
import { Scope } from './scope.interface';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_SCOPE_ENDPOINT,
  DELETE_SCOPE_ENDPOINT,
  GET_SCOPE_ENDPOINT,
  UPDATE_SCOPE_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class ScopeService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getScope(uuid: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_SCOPE_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateScope(uuid: string, name: string, description: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${UPDATE_SCOPE_ENDPOINT}/${uuid}`;

        const userData: Scope = {
          name,
          description,
        };

        return this.http.post(url, userData, { headers });
      }),
    );
  }

  createScope(name: string, description: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_SCOPE_ENDPOINT}`;
        const scopeData = {
          name,
          description,
        };
        return this.http.post(url, scopeData, { headers });
      }),
    );
  }

  deleteScope(scopeName: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${DELETE_SCOPE_ENDPOINT}/${scopeName}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
