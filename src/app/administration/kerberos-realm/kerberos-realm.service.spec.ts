import { TestBed } from '@angular/core/testing';

import { KerberosRealmService } from './kerberos-realm.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('KerberosRealmService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: KerberosRealmService = TestBed.get(KerberosRealmService);
    expect(service).toBeTruthy();
  });
});
