import { TestBed } from '@angular/core/testing';

import { LDAPClientService } from './ldap-client.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LDAPClientService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: LDAPClientService = TestBed.get(LDAPClientService);
    expect(service).toBeTruthy();
  });
});
