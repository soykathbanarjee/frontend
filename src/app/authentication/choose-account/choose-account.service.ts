import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class ChooseAccountService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getSessionUsers() {
    return this.http.get(
      this.token.appBasePath + environment.routes.LIST_SESSION_USERS,
    );
  }

  chooseUser(uuid) {
    return this.http.post(
      this.token.appBasePath + environment.routes.CHOOSE_USER,
      { uuid },
    );
  }

  logoutUser(uuid) {
    return this.http.get(
      this.token.appBasePath + environment.routes.LOGOUT + '/' + uuid,
    );
  }
}
