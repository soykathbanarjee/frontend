import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class VerifyGeneratePasswordService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  updateUser(verificationCode: string, password: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.GENERATE_PASSWORD,
      {
        verificationCode,
        password,
      },
    );
  }

  verifyEmail(verificationCode: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.VERIFY_EMAIL,
      {
        verificationCode,
      },
    );
  }
}
