import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import {
  create,
  parseCreationOptionsFromJSON,
} from '@github/webauthn-json/browser-ponyfill';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationKeysService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  addAuthKey(userUuid: string, accessToken: string) {
    const registerReqHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + accessToken);
    return this.http
      .post<any>(
        this.token.appBasePath +
          environment.routes.WEBAUTHN_REQUEST_REGISTER_ENDPOINT,
        { userUuid },
        { headers: registerReqHeaders },
      )
      .pipe(
        switchMap(challenge => {
          return from(
            create(parseCreationOptionsFromJSON({ publicKey: challenge })),
          );
        }),
        switchMap(credentials => {
          const challengeReqHeaders = new HttpHeaders()
            .append('Content-Type', 'application/json')
            .append('Authorization', 'Bearer ' + accessToken);

          return this.http.post<{ registered: string }>(
            this.token.appBasePath +
              environment.routes.WEBAUTHN_REGISTER_ENDPOINT,
            credentials,
            {
              headers: challengeReqHeaders,
            },
          );
        }),
      );
  }

  renameAuthKey(uuid: string, name: string, accessToken: string) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + accessToken);

    return this.http.post<any>(
      this.token.appBasePath +
        environment.routes.WEBAUTHN_RENAME_AUTHN_ENDPOINT +
        '/' +
        uuid,
      { name },
      { headers },
    );
  }

  removeAuthKey(uuid: string, accessToken: string) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + accessToken);

    return this.http.post<any>(
      this.token.appBasePath +
        environment.routes.WEBAUTHN_REMOVE_AUTHN_ENDPOINT +
        '/' +
        uuid,
      {},
      { headers },
    );
  }

  getAuthenticators(userUuid: string, accessToken: string) {
    const headers = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + accessToken,
    );
    return this.http.get(
      this.token.appBasePath +
        environment.routes.WEBAUTHN_AUTHNS_ENDPOINT +
        '/' +
        userUuid,
      {
        headers,
      },
    );
  }
}
