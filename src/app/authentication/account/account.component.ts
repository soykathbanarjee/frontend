import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../../auth/token/token.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  constructor(
    private readonly token: TokenService,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.token.getToken().subscribe({
      next: token => {
        this.router.navigate(['admin', 'profile']);
      },
      error: error => {
        this.token.logIn();
      },
    });
  }
}
