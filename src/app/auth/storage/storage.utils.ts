export interface IStorage {
  get(key: string): string;
  set(key: string, value: string): void;
  remove(key: string): void;
  clear(): void;
}

export class Storage implements IStorage {
  private db = sessionStorage;
  get(key: string): string {
    return this.db.getItem(key);
  }

  set(key: string, value: string): void {
    return this.db.setItem(key, value);
  }
  remove(key: string): void {
    return this.db.removeItem(key);
  }
  clear(): void {
    return this.db.clear();
  }
}
