export const STATE = 'state';
export const CODE_VERIFIER = 'code_verifier';
export const ONE_HOUR_IN_SECONDS_NUMBER = 3600;
export const ACCESS_TOKEN = 'access_token';
export const REFRESH_TOKEN = 'refresh_token';
export const EXPIRES_IN = 'expires_in';
export const ID_TOKEN = 'id_token';
export const LOGGED_IN = 'logged_in';
export const TEN_MINUTES_IN_SECONDS_NUMBER = 600;
export const AUTHORIZATION = 'Authorization';
export const CONTENT_TYPE = 'Content-Type';
export const APP_X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
export const BEARER_HEADER_PREFIX = 'Bearer ';
export const DEFAULT_LOGIN_ERROR_MSG = 'Kindly contact administrator';
