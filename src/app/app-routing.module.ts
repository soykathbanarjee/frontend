import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './authentication/account/account.component';
import { LoginComponent } from './authentication/login/login.component';
import { SignupComponent } from './authentication/signup/signup.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { VerifyGeneratePasswordComponent } from './authentication/verify-generate-password/verify-generate-password.component';
import { ChooseAccountComponent } from './authentication/choose-account/choose-account.component';
import { AuthenticationKeysComponent } from './authentication/authentication-keys/authentication-keys.component';
import { VerifyEmailComponent } from './authentication/verify-email/verify-email.component';
import { CallbackPage } from './auth/callback/callback.page';
import { AdminAuthGuard } from './administration/guards/admin-auth.guard.service';
import { AppComponent } from './app.component';
import { AppsComponent } from './administration/apps/apps.component';
import { MultifactorComponent } from './administration/multifactor/multifactor.component';
import { ProfileComponent } from './administration/profile/profile.component';
import { UpdatePhoneComponent } from './administration/update-phone/update-phone.component';
import { AuthSettingsComponent } from './administration/auth-settings/auth-settings.component';
import { ClientComponent } from './administration/client/client.component';
import { KerberosRealmComponent } from './administration/kerberos-realm/kerberos-realm.component';
import { LDAPClientComponent } from './administration/ldap-client/ldap-client.component';
import { ListingComponent } from './administration/listing/listing.component';
import { RoleComponent } from './administration/role/role.component';
import { ScopeComponent } from './administration/scope/scope.component';
import { SocialLoginComponent } from './administration/social-login/social-login.component';
import { UserComponent } from './administration/user/user.component';
import { EmailComponent } from './administration/email/email.component';
import { CloudStorageComponent } from './administration/cloud-storage/cloud-storage.component';
import { OAuth2ProviderComponent } from './administration/oauth2-provider/oauth2-provider.component';
import { NavigationComponent } from './administration/navigation/navigation.component';

export const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', component: AccountComponent },
      { path: 'login', component: LoginComponent },
      { path: 'account/choose', component: ChooseAccountComponent },
      {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthGuardService],
      },
      { path: 'signup', component: SignupComponent },
      { path: 'signup/:code', component: VerifyGeneratePasswordComponent },
      { path: 'forgot/:code', component: VerifyGeneratePasswordComponent },
      { path: 'verify', component: VerifyEmailComponent },
      { path: 'verify/:code', component: VerifyEmailComponent },
      { path: 'callback', component: CallbackPage },
    ],
  },

  {
    path: 'admin/profile',
    component: NavigationComponent,
    canActivate: [AuthGuardService],
    children: [
      // profile self-service
      {
        path: '',
        component: ProfileComponent,
      },
      {
        path: 'mfa',
        component: MultifactorComponent,
      },
      { path: 'apps', component: AppsComponent },
      {
        path: 'update_phone',
        component: UpdatePhoneComponent,
      },
      {
        path: 'keys/:userUuid',
        component: AuthenticationKeysComponent,
      },
    ],
  },

  {
    path: 'admin',
    component: NavigationComponent,
    canActivate: [AdminAuthGuard],
    children: [
      // Server Admin
      {
        path: 'list/client',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/role',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/user',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/scope',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/social_login',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/ldap_client',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/kerberos_realm',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/email',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/storage',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/oauth2_provider',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/service',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'list/service_type',
        component: ListingComponent,
        canActivate: [AdminAuthGuard],
      },
      {
        path: 'form/client/:id',
        component: ClientComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/user/:id',
        component: UserComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/role/:id',
        component: RoleComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/scope/:id',
        component: ScopeComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/social_login/:id',
        component: SocialLoginComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/ldap_client/:id',
        component: LDAPClientComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/kerberos_realm/:id',
        component: KerberosRealmComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/email/:id',
        component: EmailComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/storage/:id',
        component: CloudStorageComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'form/oauth2_provider/:id',
        component: OAuth2ProviderComponent,
        canActivateChild: [AdminAuthGuard],
      },
      {
        path: 'auth_settings',
        component: AuthSettingsComponent,
      },
    ],
  },

  // Keep at end
  { path: '', redirectTo: 'account', pathMatch: 'full' },
  { path: '**', redirectTo: 'account', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService],
})
export class AppRoutingModule {}
