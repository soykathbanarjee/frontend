function headers_to_json(req) {
  var kvpairs = '';
  for (var h in req.headers) {
    if (kvpairs.length) {
      kvpairs += ',';
    }
    kvpairs += '"' + h + '":';
    if (isNaN(req.headers[h])) {
      kvpairs += '"' + req.headers[h] + '"';
    } else {
      kvpairs += req.headers[h];
    }
  }
  return kvpairs;
}

export default { headers_to_json };
